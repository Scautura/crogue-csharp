﻿using CyberRogue.Cartography;
using CyberRogue.Core;
using System;
using System.Collections.Generic;

namespace CyberRogue.AI
{
    /// <summary>
    /// The base class for all AI.
    /// </summary>
    public abstract class AI
    {
        /// <summary>
        /// The owner of this AI object.
        /// </summary>
        public Entity owner;

        /// <summary>
        /// A function that will let the owning <see cref="Entity"/> take a turn in the game.
        /// </summary>
        /// <param name="target">A target to chase.</param>
        /// <param name="gameMap">A <see cref="GameMap"/> to work with.</param>
        /// <param name="entities">A list of <see cref="Entity"/>s to work with.</param>
        /// <returns>A list of "things" to be worked with.</returns>
        public abstract List<Tuple<string, dynamic>> TakeTurn(Entity target, GameMap gameMap, List<Entity> entities);
    }
}
