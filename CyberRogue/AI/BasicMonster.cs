﻿using CyberRogue.Cartography;
using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.AI
{
    /// <summary>
    /// AI for a basic monster.
    /// </summary>
    public class BasicMonster : AI
    {
        /// <summary>
        /// Chase or attack the target.
        /// </summary>
        /// <param name="target">Target to chase.</param>
        /// <param name="gameMap">A <see cref="GameMap"/> to work with.</param>
        /// <param name="entities">A list of <see cref="Entity"/>s to work with.</param>
        /// <returns>A list of "things" to be worked on.</returns>
        public override List<Tuple<string, dynamic>> TakeTurn(Entity target, GameMap gameMap, List<Entity> entities)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            if (gameMap.IsInFov(owner.X, owner.Y))
            {
                if (owner.DistanceTo(target) >= 2)
                {
                    owner.MoveAStar(target, entities, gameMap);
                }
                else if (target.Fighter.HP > 0)
                {
                    results.AddRange(owner.Fighter.Attack(target));
                }
            }

            return results;
        }
    }
}
