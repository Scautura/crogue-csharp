﻿using CyberRogue.Cartography;
using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.AI
{
    public class ConfusedMonster : AI
    {
        private AI PreviousAI;
        private int NumberOfTurns;
        private static Random rnd = new Random();

        public ConfusedMonster(AI previousAI, int numberOfTurns = 10)
        {
            PreviousAI = previousAI;
            NumberOfTurns = numberOfTurns;
        }

        public override List<Tuple<string, dynamic>> TakeTurn(Entity target, GameMap gameMap, List<Entity> entities)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            if (NumberOfTurns > 0)
            {
                var randomX = owner.X + rnd.Next(3) - 1;
                var randomY = owner.Y + rnd.Next(3) - 1;

                if (randomX != owner.X && randomY != owner.Y)
                {
                    owner.MoveTowards(randomX, randomY, gameMap, entities);
                }

                NumberOfTurns -= 1;
            }
            else
            {
                owner.AI = PreviousAI;
                results.Add(new Tuple<string, dynamic>("message", new Message($"The {owner.Name} is no longer confused!", Constants.Colors["red"])));
            }

            return results;
        }
    }
}
