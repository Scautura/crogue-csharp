﻿namespace CyberRogue.Graphics
{
    /// <summary>
    /// A canera to display a part of a map.
    /// </summary>
    public class Camera
    {
        private Cartography.GameMap GameMap;

        /// <summary>
        /// The section of the map to display.
        /// </summary>
        public RogueSharp.Rectangle View;

        /// <summary>
        /// Initialize the camera.
        /// </summary>
        /// <param name="gameMap">The map to view with the camera.</param>
        /// <param name="x">Left of the camera view.</param>
        /// <param name="y">Top of the camera view.</param>
        /// <param name="width">How wide the camera is.</param>
        /// <param name="height">How tall the camera is.</param>
        public Camera(Cartography.GameMap gameMap, int x, int y, int width, int height)
        {
            View = new RogueSharp.Rectangle(x, y, width, height);
            GameMap = gameMap;
        }

        public void Offset(int dx, int dy)
        {
            View.Offset(dx, dy);
        }
    }
}
