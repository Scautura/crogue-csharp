﻿using BearLib;
using CyberRogue.Core;
using System.Collections.Generic;
using RogueSharp;
using System.Linq;
using System;
using CyberRogue.Cartography;

namespace CyberRogue.Graphics
{
    /// <summary>
    /// Options for rendering order.
    /// </summary>
    public enum RenderOrder
    {
        /// <summary>
        /// Corpses, go at the bottom of any rendering.
        /// </summary>
        CORPSE = 1,

        /// <summary>
        /// Items, go in the middle of any rendering.
        /// </summary>
        ITEM = 2,

        /// <summary>
        /// Actors, go at the top of any rendering.
        /// </summary>
        ACTOR = 3
    }

    /// <summary>
    /// All rendering code.
    /// </summary>
    public class Render
    {
        /// <summary>
        /// Renders all the specified entities and map on the given layer.
        /// </summary>
        /// <param name="entities">The list of <see cref="Entity"/>s to be rendered.</param>
        /// <param name="player">The player <see cref="Entity"/>.</param>
        /// <param name="gameMap">The <see cref="Cartography.GameMap"/> to be rendered.</param>
        /// <param name="camera">The camera.</param>
        /// <param name="messageLog">The <see cref="MessageLog"/> to be rendered.</param>
        /// <param name="mouse">Current mouse co-ordinates.</param>
        /// <param name="layer">The layer to be rendered on.</param>
        public static void All(List<Entity> entities, Entity player, Cartography.GameMap gameMap, Camera camera, MessageLog messageLog, Tuple<int, int> mouse, Constants.GameStates gameState, int layer = 0)
        {
            Terminal.Layer(layer);

            Terminal.BkColor("black");
            Terminal.Clear();
            Terminal.BkColor("transparent");

            for (int x = camera.View.Left; x < camera.View.Right; x++)
            {
                for (int y = camera.View.Top; y < camera.View.Bottom; y++)
                {
                    if (x >= 0 && x < gameMap.Width && y >= 0 && y < gameMap.Height && x - camera.View.X < camera.View.Width && y - camera.View.Y < camera.View.Height)
                    {
                        var visible = gameMap.IsInFov(x, y);
                        var wall = !gameMap.IsTransparent(x, y);
                        if (visible)
                        {
                            if (wall)
                                Terminal.Print(x - camera.View.X, y - camera.View.Y, Constants.DisplayTiles ? Constants.Tiles["wallTile"] : $"[bkcolor={Constants.Colors["light_wall"]}] ");
                            else
                                Terminal.Print(x - camera.View.X, y - camera.View.Y, Constants.DisplayTiles ? Constants.Tiles["floorTile"] : $"[bkcolor={Constants.Colors["light_ground"]}] ");
                            gameMap.SetCellProperties(x, y, gameMap.IsTransparent(x, y), gameMap.IsWalkable(x, y), true);
                        }
                        else if (gameMap.IsExplored(x, y))
                        {
                            if (wall)
                                Terminal.Print(x - camera.View.X, y - camera.View.Y, $"[{(Constants.DisplayTiles ? "" : "bk")}color={Constants.Colors["dark_wall"]}]" + (Constants.DisplayTiles ? Constants.Tiles["wallTile"] : " "));
                            else
                                Terminal.Print(x - camera.View.X, y - camera.View.Y, $"[{(Constants.DisplayTiles ? "" : "bk")}color={Constants.Colors["dark_ground"]}]" + (Constants.DisplayTiles ? Constants.Tiles["floorTile"] : " "));
                        }
                    }
                }
            }

            var entitiesInRenderOrder = entities.OrderBy(entity => entity.RenderOrder);

            foreach (Entity entity in entitiesInRenderOrder)
            {
                DrawEntity(entity, gameMap.IsInFov(entity.X, entity.Y), camera, layer);
            }

            Bar(1, 1, Constants.BarWidth, "HP", player.Fighter.HP, player.Fighter.MaxHP, Constants.Colors["light_red"], Constants.Colors["darker_red"]);

            if (mouse != null)
            {
                Terminal.Print(1, Constants.PanelY, $"[color=#FF808080]{GetNamesUnderMouse(mouse.Item1, mouse.Item2, camera, gameMap, entities)}");
            }

            var y1 = 1;
            foreach(Message message in messageLog.Messages)
            {
                Terminal.Print(messageLog.X, y1 + Constants.PanelY, $"[color={message.Color}]{message.Text}");
                y1 += 1;
            }

            var title = "";
            if (gameState == Constants.GameStates.SHOW_INVENTORY || gameState == Constants.GameStates.DROP_INVENTORY)
            {
                if(gameState==Constants.GameStates.SHOW_INVENTORY)
                {
                    title = "Press the key next to an item to use it, or Esc to cancel.\n";
                }
                else
                {
                    title = "Press the key next to an item to drop it, or Esc to cancel.\n";
                }

                Menus.InventoryMenu(title, player.Inventory, 50, Constants.ScreenWidth, Constants.ScreenHeight);
            }

        }

        /// <summary>
        /// Clears all entities from the given layer.
        /// </summary>
        /// <param name="entities">The entities to be cleared.</param>
        /// <seealso cref="Entity"/> 
        /// <param name="layer">The layer for the entity to be cleared from.</param>
        public static void ClearAll(List<Entity> entities, int layer = 0)
        {
            foreach (Entity entity in entities)
            {
                ClearEntity(entity, layer);
            }
        }

        /// <summary>
        /// Draws the entity on the given layer.
        /// </summary>
        /// <param name="entity">The entity to be drawn.</param>
        /// <seealso cref="Entity"/> 
        /// <param name="visible">Whether entity is visible.</param>
        /// <param name="camera">The camera.</param>
        /// <param name="layer">The layer for the entity to be drawn on..</param>
        public static void DrawEntity(Entity entity, bool visible, Camera camera, int layer = 0)
        {
            if (visible)
            {
                Terminal.Layer(layer);
                Terminal.Print(entity.X - camera.View.X, entity.Y - camera.View.Y, $"[color={Constants.Colors[entity.Color]}]{Constants.Tiles[entity.Char]}");
            }
        }

        /// <summary>
        /// Clears the entity from a given layer.
        /// </summary>
        /// <param name="entity">The entity to be cleared.</param>
        /// <seealso cref="Entity"/> 
        /// <param name="layer">The layer for the entity to be cleared from.</param>
        public static void ClearEntity(Entity entity, int layer = 0)
        {
            Terminal.Layer(layer);
            Terminal.Print(entity.X, entity.Y, " ");
        }

        /// <summary>
        /// Render a percentage bar with the given specifications.
        /// </summary>
        /// <param name="x">X co-ordinate to render the bar.</param>
        /// <param name="y">Y co-ordinate to render the bar.</param>
        /// <param name="totalWidth">The width of the bar, in characters.</param>
        /// <param name="name">The name of the bar.</param>
        /// <param name="value">The current value.</param>
        /// <param name="maximum">The maximum value.</param>
        /// <param name="barColor">The color to fill the bar in.</param>
        /// <param name="backColor">The background color for the bar (the not-filled part).</param>
        public static void Bar(int x, int y, int totalWidth, string name, int value, int maximum, string barColor, string backColor)
        {
            var barWidth = (int)(((decimal)value / (decimal)maximum) * (decimal)totalWidth);

            Terminal.BkColor(backColor);
            Terminal.ClearArea(x, Constants.PanelY + y, totalWidth, 1);

            Terminal.BkColor(barColor);
            if(barWidth>0)
            {
                Terminal.ClearArea(x, Constants.PanelY + y, barWidth, 1);
            }

            Terminal.BkColor("transparent");
            Terminal.Color("white");

            var output = $"{name}: {value}/{maximum}";
            var offset = Terminal.Measure(output).Width;
            Terminal.Print((x + totalWidth / 2) - offset / 2, Constants.PanelY + y, output);
        }

        /// <summary>
        /// Get a list of names of <see cref="Entity"/>s under the mouse
        /// </summary>
        /// <param name="x">X co-ordinate to get names from.</param>
        /// <param name="y">Y co-ordinate to get names from.</param>
        /// <param name="camera">The current camera for the map.</param>
        /// <param name="gameMap">The <see cref="GameMap"/> to work with.</param>
        /// <param name="entities">A list of <see cref="Entity"/>s to work with.</param>
        /// <returns>A string containing a list of names of <see cref="Entity"/>s under the mouse.</returns>
        public static string GetNamesUnderMouse(int x, int y, Camera camera, GameMap gameMap, List<Entity> entities)
        {
            List<string> names = new List<string>();

            foreach(Entity entity in entities)
            {
                if (entity.X - camera.View.X == x && entity.Y - camera.View.Y == y && gameMap.IsInFov(entity.X, entity.Y))
                {
                    names.Add(entity.Name);
                }
            }

            if (names.Any())
            {
                return string.Join(", ", names).First().ToString().ToUpper() + string.Join(", ", names).Substring(1);
            }
            else
            {
                return "";
            }
        }
    }
}
