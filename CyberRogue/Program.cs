﻿using BearLib;
using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using static CyberRogue.Core.Constants;

namespace CyberRogue
{
    /// <summary>
    /// Main class for program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Player <see cref="Entity"/>.
        /// </summary>
        private static Entity Player;

        /// <summary>
        /// Game map.
        /// </summary>
        /// <seealso cref="Cartography.GameMap" />
        private static Cartography.GameMap GameMap;

        private static Graphics.Camera Camera;

        /// <summary>
        /// A list of all entities in the game.
        /// </summary>
        public static List<Entity> entities;

        private static bool FovRecompute;
        private static MessageLog MessageLog;
        private static GameStates gameState;
        private static GameStates previousGameState;

        /// <summary>
        /// Program initialization. Call once, only at start of program.
        /// </summary>
        private static void Initialize()
        {
            Constants.Init();

            Terminal.Open();
            Terminal.Set("window: title='CyberRogue', size=" + ScreenWidth.ToString() + "x" + ScreenHeight.ToString() + "; input: filter=[keyboard, mouse];");
            //Terminal.Set("font: Media/font.png, size=16x16");
            Terminal.Set("font: Media/UbuntuMono-R.ttf, size=16x16");
            Terminal.Set("0xE000: Media/Tileset16x16.png, size=16x16");

            var fighterComponent = new Components.Fighter(hp: 30, defense: 2, power: 5);
            var inventoryComponent = new Components.Inventory(26);
            Player = new Entity(0, 0, "playerTile", "white", "Player", blocks: true, renderOrder: Graphics.RenderOrder.ACTOR, fighter: fighterComponent, inventory: inventoryComponent);

            entities = new List<Entity>();
            entities.Add(Player);

            MessageLog = new MessageLog(MessageX, MessageWidth, MessageHeight);

            GameMap = new Cartography.GameMap(MapWidth, MapHeight);
            GameMap.MakeMap(MaxRooms, RoomMinSize, RoomMaxSize, Player, entities, MaxMonstersPerRoom, MaxItemsPerRoom);

            gameState = GameStates.PLAYERS_TURN;
            previousGameState = gameState;

            Camera = new Graphics.Camera(GameMap, Player.X - (CameraWidth / 2), Player.Y - (CameraHeight / 2), CameraWidth, CameraHeight);

            FovRecompute = true;
        }

        /// <summary>
        /// Main loop. Runs until program finishes.
        /// </summary>
        private static void MainLoop()
        {
            bool exit = false;

            Tuple<int, int> mouse = null;

            Entity targetingItem = null;

            while (!exit)
            {
                if (FovRecompute)
                {
                    GameMap.ComputeFov(Player.X, Player.Y, FovRadius, FovLightWalls);
                }

                Terminal.Composition(Constants.DisplayTiles);
                Graphics.Render.All(entities, Player, GameMap, Camera, MessageLog, mouse, gameState);
                Terminal.Refresh();
                //Terminal.ClearArea(0, 0, CameraWidth, CameraHeight);
                //Terminal.Composition(false);
                Graphics.Render.ClearAll(entities);

                var keys = Terminal.Read();
                if (keys == Terminal.TK_MOUSE_MOVE)
                {
                    mouse = new Tuple<int, int>(Terminal.State(Terminal.TK_MOUSE_X), Terminal.State(Terminal.TK_MOUSE_Y));
                    //Console.WriteLine($"{mouse.Item1}, {mouse.Item2}");
                }
                

                var read = Controls.HandleKeys(keys, gameState);
                var mouse_action = Controls.HandleMouse(mouse, keys);

                exit = read.ContainsKey("exit");
                var move = read.ContainsKey("move") ? read["move"] : null;
                var pickup = read.ContainsKey("pickup") ? read["pickup"] : null;
                var showInventory = read.ContainsKey("show_inventory") ? read["show_inventory"] : null;
                var dropInventory = read.ContainsKey("drop_inventory") ? read["drop_inventory"] : null;
                var inventoryIndex = read.ContainsKey("inventory_index") ? read["inventory_index"] : null;
                var leftClick = mouse_action.ContainsKey("left_click") ? mouse_action["left_click"] : null;
                var rightClick = mouse_action.ContainsKey("right_click") ? mouse_action["right_click"] : null;
                var switchTiles = read.ContainsKey("switch_tiles") ? read["switch_tiles"] : null;

                List<Tuple<string, dynamic>> playerTurnResults = new List<Tuple<string, dynamic>>();

                if (move != null && gameState == GameStates.PLAYERS_TURN)
                {
                    int destinationX = move[0] + Player.X;
                    int destinationY = move[1] + Player.Y;

                    if (!GameMap.IsBlocked(destinationX, destinationY))
                    {
                        Entity target = Entities.BlockingAtLocation(entities, destinationX, destinationY);

                        if (target != null)
                            playerTurnResults.AddRange(Player.Fighter.Attack(target));
                        else
                        {
                            Player.Move(move[0], move[1]);
                            Camera.Offset(move[0], move[1]);
                            FovRecompute = true;
                        }

                        gameState = GameStates.ENEMY_TURN;
                    }
                }

                var pickedUp = false;
                if (pickup != null && gameState == GameStates.PLAYERS_TURN)
                {
                    foreach (Entity entity in entities)
                    {
                        if (entity.Item != null && entity.X == Player.X && entity.Y == Player.Y)
                        {
                            var pickupResults = Player.Inventory.AddItem(entity);
                            playerTurnResults.AddRange(pickupResults);

                            pickedUp = true;
                            break;
                        }
                    }
                    if (!pickedUp)
                    {
                        MessageLog.AddMessage(new Message("There is nothing here to pick up.", Colors["yellow"]));
                    }
                }

                if(showInventory != null)
                {
                    previousGameState = gameState;
                    gameState = GameStates.SHOW_INVENTORY;
                }

                if(dropInventory != null)
                {
                    previousGameState = gameState;
                    gameState = GameStates.DROP_INVENTORY;
                }

                if (inventoryIndex != null && previousGameState != GameStates.PLAYER_DEAD && inventoryIndex < Player.Inventory.Items.Count)
                {
                    var item = Player.Inventory.Items[inventoryIndex];

                    if (gameState == GameStates.SHOW_INVENTORY)
                    {
                        playerTurnResults.AddRange(Player.Inventory.Use(item, new Dictionary<string, object>{ { "entities", entities }, { "game_map", GameMap } }));
                    }
                    else if (gameState == GameStates.DROP_INVENTORY)
                    {
                        playerTurnResults.AddRange(Player.Inventory.DropItem(item));
                    }
                }

                if (switchTiles != null)
                {
                    DisplayTiles=!DisplayTiles;
                    Settings.Default.DisplayTiles = DisplayTiles;
                    Settings.Default.Save();
                    Init();
                }

                if (gameState == GameStates.TARGETING)
                {
                    if (leftClick != null)
                    {
                        var itemUseResults = Player.Inventory.Use(targetingItem, new Dictionary<string, object> { { "entities", entities }, { "game_map", GameMap }, { "target_x", leftClick.Item1 }, { "target_y", leftClick.Item2 }, { "camera", Camera } });
                        playerTurnResults.AddRange(itemUseResults);
                    }
                    else if (rightClick != null)
                    {
                        playerTurnResults.Add(new Tuple<string, dynamic>("targeting_cancelled", true));
                    }
                }

                if (exit)
                {
                    if (gameState == GameStates.SHOW_INVENTORY || gameState == GameStates.DROP_INVENTORY)
                    {
                        gameState = previousGameState;
                        exit = false;
                    }
                    else if (gameState == GameStates.TARGETING)
                    {
                        playerTurnResults.Add(new Tuple<string, dynamic>("targeting_cancelled", true));
                        exit = false;
                    }
                }

                foreach (Tuple<string, dynamic> playerTurnResult in playerTurnResults)
                {
                    var message = (playerTurnResult.Item1 == "message") ? playerTurnResult.Item2 : null;
                    var deadEntity = (playerTurnResult.Item1 == "dead") ? playerTurnResult.Item2 : null;
                    var itemAdded = (playerTurnResult.Item1 == "item_added") ? playerTurnResult.Item2 : null;
                    var itemConsumed = (playerTurnResult.Item1 == "consumed") ? playerTurnResult.Item2 : null;
                    var itemDropped = (playerTurnResult.Item1 == "item_dropped") ? playerTurnResult.Item2 : null;
                    var targeting = (playerTurnResult.Item1 == "targeting") ? playerTurnResult.Item2 : null;
                    var targetingCancelled = (playerTurnResult.Item1 == "targeting_cancelled") ? playerTurnResult.Item2 : null;

                    if (message != null)
                    {
                        MessageLog.AddMessage(message);
                    }

                    if(targetingCancelled!=null)
                    {
                        gameState = previousGameState;

                        MessageLog.AddMessage(new Message("Targeting cancelled"));
                    }

                    if (deadEntity != null)
                    {
                        if (deadEntity == Player)
                        {
                            message = Entity.KillPlayer(Player);
                            gameState = GameStates.PLAYER_DEAD;
                        }
                        else
                        {
                            message = Entity.KillMonster(deadEntity);
                        }

                        MessageLog.AddMessage(message);
                    }

                    if (itemAdded != null)
                    {
                        entities.Remove(itemAdded);

                        gameState = GameStates.ENEMY_TURN;
                    }

                    if (itemConsumed != null)
                    {
                        gameState = GameStates.ENEMY_TURN;
                    }

                    if(targeting!=null)
                    {
                        previousGameState = GameStates.PLAYERS_TURN;
                        gameState = GameStates.TARGETING;

                        targetingItem = targeting;

                        MessageLog.AddMessage(targetingItem.Item.TargetingMessage);
                    }

                    if (itemDropped != null)
                    {
                        entities.Add(itemDropped);
                        gameState = GameStates.ENEMY_TURN;
                    }
                }

                if (gameState == GameStates.ENEMY_TURN)
                {
                    foreach (Entity entity in entities)
                        if (entity.AI != null)
                        {
                            List<Tuple<string, dynamic>> enemyTurnResults = entity.AI.TakeTurn(Player, GameMap, entities);

                            foreach (Tuple<string, dynamic> enemyTurnResult in enemyTurnResults)
                            {
                                var message = enemyTurnResult.Item1 == "message" ? enemyTurnResult.Item2 : null;
                                var deadEntity = enemyTurnResult.Item1 == "dead" ? enemyTurnResult.Item2 : null;

                                if (deadEntity != null)
                                {
                                    if (deadEntity == Player)
                                    {
                                        message = Entity.KillPlayer(Player);
                                        gameState = GameStates.PLAYER_DEAD;
                                    }
                                    else
                                    {
                                        message = Entity.KillMonster(deadEntity);
                                    }

                                }

                                MessageLog.AddMessage(message);

                                if (gameState == GameStates.PLAYER_DEAD)
                                {
                                    break;
                                }
                            }
                        }
                    if (gameState != GameStates.PLAYER_DEAD)
                    {
                        gameState = GameStates.PLAYERS_TURN;
                    }
                }
            }

            Terminal.Close();
        }

        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args">A string of arguments to pass to the program.</param>
        static void Main(string[] args)
        {
            Initialize();
            MainLoop();
        }
    }
}
