﻿using BearLib;
using CyberRogue.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.Core
{
    public class Menus
    {
        public static void Menu(string header, List<string> options, int width, int screenWidth, int screenHeight, int layer = 0)
        {
            Terminal.Layer(layer);
            if (options.Count > 26)
            {
                throw new Exception("Cannot have a menu with more than 26 options.");
            }

            var headerHeight = Terminal.Measure(new System.Drawing.Size(width, screenHeight), header).Height;
            var height = headerHeight + options.Count;
            
            int x = screenWidth / 2 - width / 2;
            int y = screenHeight / 2 - height / 2;
            Terminal.Composition(false);
            Terminal.BkColor("#80000000");
            Terminal.ClearArea(x, y, width, height);
            Terminal.Print(new System.Drawing.Rectangle(x, y, width, height), header);

            y += headerHeight;
            char letterIndex = 'a';

            foreach (string optionText in options)
            {
                var text = $"({letterIndex}) {optionText}";
                Terminal.ClearArea(x, y, width, 1);
                Terminal.Print(x, y, text);
                y += 1;
                letterIndex++;
            }
            Terminal.BkColor("transparent");
        }

        public static void InventoryMenu(string header, Inventory inventory, int inventoryWidth, int screenWidth, int screenHeight, int layer=0)
        {
            var options = new List<string>();
            if (inventory.Items.Count == 0)
            {
                options.Add("Inventory is empty.");
            }
            else
            {
                foreach(Entity item in inventory.Items)
                {
                    options.Add(item.Name);
                }
            }

            Menu(header, options, inventoryWidth, screenWidth, screenHeight, layer);
        }
    }
}
