﻿using System.Collections;
using System.Collections.Generic;

namespace CyberRogue.Core
{
    /// <summary>
    /// A system to log <see cref="Message"/>s taking into account their width. 
    /// </summary>
    public class MessageLog
    {
        /// <summary>
        /// X position to display the message log on the screen.
        /// </summary>
        public int X;

        /// <summary>
        /// How wide the message log is, in characters.
        /// </summary>
        public int Width;

        /// <summary>
        /// How tall the message log is, in characters.
        /// </summary>
        public int Height;

        /// <summary>
        /// A list of <see cref="Message"/>s to be displayed.
        /// </summary>
        public List<Message> Messages = new List<Message>();

        /// <summary>
        /// Initialize the message log.
        /// </summary>
        /// <param name="x">X position to display the message log on screen.</param>
        /// <param name="width">How wide the message log is, in characters.</param>
        /// <param name="height">How tall the message log is, in characters.</param>
        public MessageLog(int x, int width, int height)
        {
            X = x;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Add a <see cref="Message"/> to the message log.
        /// </summary>
        /// <param name="message">The message to be added.</param>
        public void AddMessage(Message message)
        {
            var newMsgLines = Wrap(message.Text, Width);

            foreach(string line in newMsgLines)
            {
                if(Messages.Count==Height)
                {
                    Messages.RemoveAt(0);
                }

                Messages.Add(new Message(line, message.Color));
            }
        }

        /// <summary>
        /// Wrap a string at a certain width.
        /// </summary>
        /// <param name="text">The string to be wrapped.</param>
        /// <param name="maxLength">How wide to wrap the string, in characters</param>
        /// <returns>An array of strings, no wider than <paramref name="maxLength"/> wide.</returns>
        public static string[] Wrap(string text, int maxLength)
        {
            text = text.Replace("\n", " ");
            text = text.Replace("\r", " ");
            text = text.Replace(".", ". ");
            text = text.Replace(">", "> ");
            text = text.Replace("\t", " ");
            text = text.Replace(",", ", ");
            text = text.Replace(";", "; ");
            text = text.Replace(" ", " ");

            string[] Words = text.Split(' ');
            int currentLineLength = 0;
            ArrayList Lines = new ArrayList(text.Length / maxLength);
            string currentLine = "";
            bool InTag = false;

            foreach (string currentWord in Words)
            {
                //ignore html
                if (currentWord.Length > 0)
                {

                    if (currentWord.Substring(0, 1) == "<")
                        InTag = true;

                    if (InTag)
                    {
                        //handle filenames inside html tags
                        if (currentLine.EndsWith("."))
                        {
                            currentLine += currentWord;

                        }

                        else
                            currentLine += " " + currentWord;
                        if (currentWord.IndexOf(">") > -1)
                            InTag = false;

                    }

                    else
                    {
                        if (currentLineLength + currentWord.Length + 1 < maxLength)
                        {
                            currentLine += " " + currentWord;
                            currentLineLength += (currentWord.Length + 1);
                        }

                        else
                        {
                            Lines.Add(currentLine);
                            currentLine = currentWord;
                            currentLineLength = currentWord.Length;

                        }
                    }
                }
            }

            if (currentLine != "")
                Lines.Add(currentLine);
            string[] textLinesStr = new string[Lines.Count];
            Lines.CopyTo(textLinesStr, 0);
            return textLinesStr;
        }
    }

    /// <summary>
    /// An object holding a message and the color to display it.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// The text of the message (to be displayed).
        /// </summary>
        public string Text;

        /// <summary>
        /// The color to display the message in.
        /// </summary>
        public string Color;

        /// <summary>
        /// Initialize a message.
        /// </summary>
        /// <param name="text">The text of the message.</param>
        /// <param name="color">The color to display the message.</param>
        public Message(string text, string color = "#FFFFFFFF")
        {
            Text = text;
            Color = color;
        }
    }
}
