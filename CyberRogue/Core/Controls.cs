﻿using BearLib;
using System;
using System.Collections.Generic;

namespace CyberRogue.Core
{
    /// <summary>
    /// Handles all controls.
    /// </summary>
    public class Controls
    {
        public static Dictionary<string, dynamic> HandleKeys(int key, Constants.GameStates gameState)
        {
            if (gameState == Constants.GameStates.PLAYERS_TURN)
            {
                return HandlePlayerTurnKeys(key);
            }
            else if (gameState == Constants.GameStates.PLAYER_DEAD)
            {
                return HandlePlayerDeadKeys(key);
            }
            else if (gameState == Constants.GameStates.TARGETING)
            {
                return HandleTargetingKeys(key);
            }
            else if (gameState == Constants.GameStates.SHOW_INVENTORY || gameState == Constants.GameStates.DROP_INVENTORY)
            {
                return HandleInventoryKeys(key);
            }


            return new Dictionary<string, dynamic>();
        }


        /// <summary>
        /// Handles the keys during the player turn.
        /// </summary>
        /// <param name="key">The key to be read.</param>
        /// <returns>A dictionary to be acted upon.</returns>
        public static Dictionary<string, dynamic> HandlePlayerTurnKeys(int key)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            switch (key)
            {
                case Terminal.TK_ESCAPE:
                case Terminal.TK_CLOSE:
                    result.Add("exit", true);
                    break;
                case Terminal.TK_KP_9:
                case Terminal.TK_U:
                    result.Add("move", new int[2] { 1, -1 });
                    break;
                case Terminal.TK_KP_8:
                case Terminal.TK_K:
                    result.Add("move", new int[2] { 0, -1 });
                    break;
                case Terminal.TK_KP_7:
                case Terminal.TK_Y:
                    result.Add("move", new int[2] { -1, -1 });
                    break;
                case Terminal.TK_KP_6:
                case Terminal.TK_L:
                    result.Add("move", new int[2] { 1, 0 });
                    break;
                case Terminal.TK_KP_4:
                case Terminal.TK_H:
                    result.Add("move", new int[2] { -1, 0 });
                    break;
                case Terminal.TK_KP_3:
                case Terminal.TK_N:
                    result.Add("move", new int[2] { 1, 1 });
                    break;
                case Terminal.TK_KP_2:
                case Terminal.TK_J:
                    result.Add("move", new int[2] { 0, 1 });
                    break;
                case Terminal.TK_KP_1:
                case Terminal.TK_B:
                    result.Add("move", new int[2] { -1, 1 });
                    break;
                case Terminal.TK_G:
                    result.Add("pickup", true);
                    break;
                case Terminal.TK_I:
                    result.Add("show_inventory", true);
                    break;
                case Terminal.TK_D:
                    result.Add("drop_inventory", true);
                    break;
                case Terminal.TK_T:
                    result.Add("switch_tiles", true);
                    break;
                default:
                    result.Add("none", false);
                    break;
            }

            return result;
        }

        public static Dictionary<string, dynamic> HandlePlayerDeadKeys(int key)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            switch (key)
            {
                case Terminal.TK_ESCAPE:
                case Terminal.TK_CLOSE:
                    result.Add("exit", true);
                    break;
                case Terminal.TK_I:
                    result.Add("show_inventory", true);
                    break;
                default:
                    result.Add("none", false);
                    break;
            }

            return result;
        }

        public static Dictionary<string, dynamic> HandleInventoryKeys(int key)
        {
            var index = key - Terminal.TK_A;
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            if (index >= 0 && index <= 26)
            {
                result.Add("inventory_index", index);
            }
            else if (key == Terminal.TK_ESCAPE)
            {
                result.Add("exit", true);
            }
            return result;
        }

        public static Dictionary<string, dynamic> HandleTargetingKeys(int key)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            switch (key)
            {
                case Terminal.TK_ESCAPE:
                    result.Add("exit", true);
                    break;
            }

            return result;
        }

        public static Dictionary<string, dynamic> HandleMouse(Tuple<int, int> mouse, int key)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            if (key == Terminal.TK_MOUSE_LEFT)
            {
                result.Add("left_click", mouse);
            }
            else if (key == Terminal.TK_MOUSE_RIGHT)
            {
                result.Add("right_click", mouse);
            }

            return result;
        }
    }
}
