﻿using System.Collections.Generic;
using System.Runtime.Remoting.Channels;

namespace CyberRogue.Core
{
    /// <summary>
    /// A class containing all constants used in the game.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The screen width.
        /// </summary>
        public const int ScreenWidth = 80;

        /// <summary>
        /// The screen height.
        /// </summary>
        public const int ScreenHeight = 50;

        /// <summary>
        /// How wide the HP bar is, in characters.
        /// </summary>
        public const int BarWidth = 20;

        /// <summary>
        /// How tall the character stats panel is, in characters.
        /// </summary>
        public const int PanelHeight = 7;

        /// <summary>
        /// Y position for the character stats panel.
        /// </summary>
        public const int PanelY = ScreenHeight - PanelHeight;

        /// <summary>
        /// X position for the message log.
        /// </summary>
        public const int MessageX = BarWidth + 2;

        /// <summary>
        /// Maximum width for messages in the message log, in characters.
        /// </summary>
        public const int MessageWidth = ScreenWidth - BarWidth - 2;

        /// <summary>
        /// How tall the messages log is, in characters.
        /// </summary>
        public const int MessageHeight = PanelHeight - 1;

        /// <summary>
        /// The map width.
        /// </summary>
        public const int MapWidth = 100;

        /// <summary>
        /// The map height.
        /// </summary>
        public const int MapHeight = 100;

        /// <summary>
        /// The maximum room size
        /// </summary>
        public const int RoomMaxSize = 10;

        /// <summary>
        /// The minimum room size.
        /// </summary>
        public const int RoomMinSize = 7;

        /// <summary>
        /// The maximum number of rooms.
        /// </summary>
        public const int MaxRooms = 30;

        /// <summary>
        /// Field of View radius.
        /// </summary>
        public const int FovRadius = 10;

        /// <summary>
        /// Whether to light walls or not with the FoV algorithm.
        /// </summary>
        public const bool FovLightWalls = true;

        /// <summary>
        /// Maximum number of monsters to generate in a room.
        /// </summary>
        public const int MaxMonstersPerRoom = 3;

        /// <summary>
        /// Maximum number of items to generate in a room.
        /// </summary>
        public const int MaxItemsPerRoom = 2;

        /// <summary>
        /// Whether to create rooms that fill in the BSP node
        /// </summary>
        public const bool FullRooms = false;

        /// <summary>
        /// Whether to display tiles or ASCII.
        /// </summary>
        public static bool DisplayTiles = Settings.Default.DisplayTiles;

        /// <summary>
        /// Map display width.
        /// </summary>
        public const int CameraWidth = 80;

        /// <summary>
        /// Map display height.
        /// </summary>
        public const int CameraHeight = 43;

        /// <summary>
        /// A set of game states.
        /// </summary>
        public enum GameStates
        {
            /// <summary>
            /// Game state for player's turn.
            /// </summary>
            PLAYERS_TURN = 1,

            /// <summary>
            /// Game state for enemy's turn.
            /// </summary>
            ENEMY_TURN = 2,

            /// <summary>
            /// Game state for the player being dead.
            /// </summary>
            PLAYER_DEAD = 3,

            /// <summary>
            /// Game state for showing inventory to the player.
            /// </summary>
            SHOW_INVENTORY = 4,

            /// <summary>
            /// Game state for dropping an item from the inventory.
            /// </summary>
            DROP_INVENTORY = 5,

            /// <summary>
            /// Game state for targeting a space for an effect.
            /// </summary>
            TARGETING = 6
        }

        /// <summary>
        /// Map generation routines.
        /// </summary>
        public enum MapGeneration
        {
            /// <summary>
            /// Default map generation routine (make a room, make a corridor to previous room)
            /// </summary>
            DEFAULT = 0,

            /// <summary>
            /// BSP tree map generation.
            /// </summary>
            BSP = 1
        }

        /// <summary>
        /// A dictionary of colors.
        /// </summary>
        public static Dictionary<string, string> Colors;

        /// <summary>
        /// A dictionary of graphical tiles.
        /// </summary>
        public static Dictionary<string, string> Tiles;

        /// <summary>
        /// Initialize constants that cannot be declared in declarations (Dictionaries for <see cref="Colors"/> and <see cref="Tiles"/>).
        /// </summary>
        public static void Init()
        {
            Colors = new Dictionary<string, string>
            {
                {"dark_wall", DisplayTiles ? "#FF808080" : "#FF000064"},
                {"dark_ground", DisplayTiles ? "#FF808080" : "#FF323296"},
                {"light_wall", DisplayTiles ? "#FFFFFFFF" : "#FF826E32"},
                {"light_ground", DisplayTiles ? "#FFFFFFFF" : "#FFC8B432"},
                {"orc", DisplayTiles ? "#FFFFFFFF" : "#FF408040"},
                {"troll", DisplayTiles ? "#FFFFFFFF" : "#FF008000"},
                {"corpse", "#FFBF0000"},
                {"potion", DisplayTiles ? "#FFFFFFFF" : "#FF7F00FF"},
                {"scroll", DisplayTiles ? "#FFFFFFFF" : "#FFFFFF00"},
                {"light_red", "#FFFF7373"},
                {"darker_red", "#FF800000"},
                {"yellow", "#FFFFFF00"},
                {"blue", "#FF0000FF"},
                {"green", "#FF00FF00"},
                {"red", "#FFFF0000"},
                {"orange", "#FFFF7F00"},
                {"light_cyan", "#FF73FFFF"},
                {"light_green", "#FF73FF73"},
                {"white", "white" }
            };

            Tiles = new Dictionary<string, string>
            {
                {"wallTile", "[U+E000]"},
                {"floorTile", "[U+E001]"},
                {"playerTile", DisplayTiles ? "[U+E002]" : "@"},
                {"orcTile", DisplayTiles ? "[U+E003]" : "o"},
                {"trollTile", DisplayTiles ? "[U+E004]" : "T"},
                {"scrollTile", DisplayTiles ? "[U+E005]" : "#"},
                {"potionTile", DisplayTiles ? "[U+E006]" : "!"},
                {"swordTile", "[U+E007]"},
                {"shieldTile", "[U+E008]"},
                {"stairsTile", "[U+E009]"},
                {"daggerTile", "[U+E010]"},
                {"corpse", "%" }
            };
        }
    }
}
