﻿using CyberRogue.Cartography;
using CyberRogue.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.Core
{
    public class ItemFunctions
    {
        public static List<Tuple<string, dynamic>> Heal(Entity caster, Dictionary<string, dynamic> kwargs = null)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();
            //Dictionary<string, object> args = ObjectToDictionaryRegistry(kwargs);

            int amount = kwargs.ContainsKey("amount")?(int)kwargs["amount"]:4;

            if(caster.Fighter.HP==caster.Fighter.MaxHP)
            {
                results.Add(new Tuple<string, dynamic>("consumed", false));
                results.Add(new Tuple<string, dynamic>("message", new Message("You are already at full health", Constants.Colors["yellow"])));
            }
            else
            {
                caster.Fighter.Heal(amount);
                results.Add(new Tuple<string, dynamic>("consumed", true));
                results.Add(new Tuple<string, dynamic>("message", new Message("Your wounds start to feel better!", Constants.Colors["green"])));
            }

            return results;
        }

        public static List<Tuple<string, dynamic>> CastLightning(Entity caster, Dictionary<string, dynamic> kwargs = null)
        {
            List<Entity> entities = kwargs.ContainsKey("entities") ? kwargs["entities"] : null;
            GameMap gameMap = kwargs.ContainsKey("game_map") ? kwargs["game_map"] : null;
            int damage = kwargs.ContainsKey("damage") ? kwargs["damage"] : 0;
            int maxRange = kwargs.ContainsKey("maximum_range") ? kwargs["maximum_range"] : 0;

            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            Entity target = null;
            int closestDistance = maxRange + 1;

            if (entities != null && gameMap != null && damage != 0 && maxRange != 0)
            {
                foreach(Entity entity in entities)
                {
                    if (entity.Fighter != null && entity != caster && gameMap.IsInFov(entity.X, entity.Y))
                    {
                        var distance = caster.DistanceTo(entity);

                        if (distance < closestDistance)
                        {
                            target = entity;
                            closestDistance = distance;
                        }
                    }
                }

                if(target!=null)
                {
                    results.Add(new Tuple<string, dynamic>("consumed", true));
                    results.Add(new Tuple<string, dynamic>("message", new Message($"A lightning bolt strikes the {target.Name} with a loud thunder! The damage is {damage}")));
                    results.AddRange(target.Fighter.TakeDamage(damage));
                }
                else
                {
                    results.Add(new Tuple<string, dynamic>("consumed", false));
                    results.Add(new Tuple<string, dynamic>("message", new Message("No enemy is close enough to strike.", Constants.Colors["red"])));
                }
            }

            return results;
        }

        public static List<Tuple<string, dynamic>> CastFireball(Entity caster, Dictionary<string, dynamic> kwargs = null)
        {
            List<Entity> entities = kwargs.ContainsKey("entities") ? kwargs["entities"] : null;
            GameMap gameMap = kwargs.ContainsKey("game_map") ? kwargs["game_map"] : null;
            int damage = kwargs.ContainsKey("damage") ? kwargs["damage"] : 0;
            int radius = kwargs.ContainsKey("radius") ? kwargs["radius"] : 0;
            int targetX = kwargs.ContainsKey("target_x") ? kwargs["target_x"] : -1;
            int targetY = kwargs.ContainsKey("target_y") ? kwargs["target_y"] : -1;
            Camera camera = kwargs.ContainsKey("camera") ? kwargs["camera"] : null;

            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            if (entities != null && gameMap != null && damage != 0 && radius != 0 && targetX != -1 && targetY != -1 && camera != null)
            {
                if (!gameMap.IsInFov(targetX + camera.View.X, targetY + camera.View.Y))
                {
                    results.Add(new Tuple<string, dynamic>("consumed", false));
                    results.Add(new Tuple<string, dynamic>("message", new Message("You cannot target a tile outside your field of view.", Constants.Colors["yellow"])));
                    return results;
                }

                results.Add(new Tuple<string, dynamic>("consumed", true));
                results.Add(new Tuple<string, dynamic>("message", new Message($"The fireball explodes, burning everything within {radius} tiles!", Constants.Colors["orange"])));

                foreach(Entity entity in entities)
                {
                    if (entity.Distance(targetX + camera.View.X, targetY + camera.View.Y) <= radius && entity.Fighter != null)
                    {
                        results.Add(new Tuple<string, dynamic>("message", new Message($"The {entity.Name} gets burned for {damage} hit points.", Constants.Colors["orange"])));
                        results.AddRange(entity.Fighter.TakeDamage(damage));
                    }
                }
            }

            return results;
        }

        public static List<Tuple<string, dynamic>> CastConfuse(Entity caster, Dictionary<string, dynamic> kwargs = null)
        {
            List<Entity> entities = kwargs.ContainsKey("entities") ? kwargs["entities"] : null;
            GameMap gameMap = kwargs.ContainsKey("game_map") ? kwargs["game_map"] : null;
            int targetX = kwargs.ContainsKey("target_x") ? kwargs["target_x"] : -1;
            int targetY = kwargs.ContainsKey("target_y") ? kwargs["target_y"] : -1;
            Camera camera = kwargs.ContainsKey("camera") ? kwargs["camera"] : null;

            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            if(entities != null && gameMap != null && targetX != -1 && targetY != -1 && camera != null)
            {
                if (!gameMap.IsInFov(targetX + camera.View.X, targetY + camera.View.Y))
                {
                    results.Add(new Tuple<string, dynamic>("consumed", false));
                    results.Add(new Tuple<string, dynamic>("message", new Message("You cannot target a tile outside your field of view.", Constants.Colors["yellow"])));
                    return results;
                }
                var confused = false;
                foreach (Entity entity in entities)
                {
                    if(entity.X==targetX+camera.View.X &&entity.Y==targetY+camera.View.Y && entity.AI!=null)
                    {
                        var confusedAI = new AI.ConfusedMonster(entity.AI, 10);

                        confusedAI.owner = entity;
                        entity.AI = confusedAI;

                        results.Add(new Tuple<string, dynamic>("consumed", true));
                        results.Add(new Tuple<string, dynamic>("message", new Message($"The eyes of the {entity.Name} look vacant, as he starts to stumble around!", Constants.Colors["light_green"])));

                        confused = true;
                        break;
                    }
                }

                if(!confused)
                {
                    results.Add(new Tuple<string, dynamic>("consumed", false));
                    results.Add(new Tuple<string, dynamic>("message", new Message("There is no targetable enemy at that location.", Constants.Colors["yellow"])));
                }
            }

            return results;
        }
    }
}
