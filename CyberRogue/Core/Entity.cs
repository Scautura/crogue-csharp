﻿using BearLib;
using CyberRogue.AI;
using CyberRogue.Cartography;
using CyberRogue.Components;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CyberRogue.Core
{
    /// <summary>
    /// A representation of an entity in the game.
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// The x co-ordinate of the entity.
        /// </summary>
        public int X;

        /// <summary>
        /// The y co-ordinate of the entity.
        /// </summary>
        public int Y;

        /// <summary>
        /// The character representation of the entity.
        /// </summary>
        public string Char;

        /// <summary>
        /// The color to display the character representation.
        /// </summary>
        public string Color;

        /// <summary>
        /// A name for the entity.
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether the entity blocks movement.
        /// </summary>
        public bool Blocks;

        /// <summary>
        /// The rendering order for the entity.
        /// </summary>
        public Graphics.RenderOrder RenderOrder;

        /// <summary>
        /// All entities that can fight/be damaged need a Fighter object.
        /// </summary>
        public Fighter Fighter;

        /// <summary>
        /// AI for the entity, if it has one.
        /// </summary>
        public AI.AI AI;

        public Item Item;

        public Inventory Inventory;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="x">The x co-ordinate.</param>
        /// <param name="y">The y co-ordinate.</param>
        /// <param name="character">The character representation.</param>
        /// <param name="color">The color representation.</param>
        /// <param name="name">A name for the entity.</param>
        /// <param name="blocks">Whether the entity blocks movement.</param>
        /// <param name="renderOrder">The render order for the entity.</param>
        /// <param name="fighter">The <see cref="Fighter"/> object for the entity, if it has one.</param>
        /// <param name="ai">The <see cref="AI.AI"/> object for the entity, if it has one.</param>
        public Entity(int x, int y, string character, string color, string name, bool blocks=false, Graphics.RenderOrder renderOrder=Graphics.RenderOrder.CORPSE, Fighter fighter=null, AI.AI ai=null, Item item=null, Inventory inventory=null)
        {
            X = x;
            Y = y;
            Char = character;
            Color = color;
            Name = name;
            Blocks = blocks;
            RenderOrder = renderOrder;
            Fighter = fighter;
            AI = ai;
            Item = item;
            Inventory = inventory;

            if (Fighter != null)
                Fighter.owner = this;

            if (AI != null)
                AI.owner = this;

            if (Item != null)
                Item.owner = this;

            if (Inventory != null)
                Inventory.owner = this;
        }

        /// <summary>
        /// Moves the entity by a certain amount (can be negative).
        /// </summary>
        /// <param name="dx">The x distance to move (can be negative).</param>
        /// <param name="dy">The y distance to move (can be negative).</param>
        public void Move(int dx, int dy)
        {
            this.X += dx;
            this.Y += dy;
        }

        /// <summary>
        /// Move towards a given target cell.
        /// </summary>
        /// <param name="target_x">X co-ordinate to move towards.</param>
        /// <param name="target_y">Y co-ordinate to move towards.</param>
        /// <param name="gameMap">A <see cref="GameMap"/> object to work with.</param>
        /// <param name="entities">A list of <see cref="Entity"/>s to work with.</param>
        public void MoveTowards(int target_x, int target_y, GameMap gameMap, List<Entity> entities)
        {
            var dx = target_x - X;
            var dy = target_y - Y;
            var distance = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

            dx = (int)Math.Round(dx / distance);
            dy = (int)Math.Round(dy / distance);

            if (!(gameMap.IsBlocked(X + dx, Y + dy) || Entities.BlockingAtLocation(entities, X + dx, Y + dy) != null))
            {
                Move(dx, dy);
            }
        }

        /// <summary>
        /// Move towards a target <see cref="Entity"/> using A* pathfinding.
        /// </summary>
        /// <param name="target">The target <see cref="Entity"/>.</param>
        /// <param name="entities">A list of entities to avoid.</param>
        /// <param name="gameMap">A <see cref="GameMap"/> to work with.</param>
        public void MoveAStar(Entity target, List<Entity> entities, GameMap gameMap)
        {
            foreach(Entity entity in entities)
            {
                if (entity != this && entity != target)
                {
                    gameMap.SetCellProperties(entity.X, entity.Y, gameMap.IsTransparent(entity.X, entity.Y), Entities.BlockingAtLocation(entities, entity.X, entity.Y) == null);
                }
            }

            var pathFinder = new RogueSharp.PathFinder(gameMap, 1.41);
            RogueSharp.Path path = null;

            try
            {
                path = pathFinder.ShortestPath(gameMap.GetCell(X, Y), gameMap.GetCell(target.X, target.Y));
            }
            catch (RogueSharp.PathNotFoundException)
            {
                //Console.WriteLine($"{Name} waits for a turn.");
            }

            if (path != null)
            {
                try
                {
                    var step=path.StepForward();

                    X = step.X;
                    Y = step.Y;
                }
                catch (RogueSharp.NoMoreStepsException)
                {
                    //Console.WriteLine($"{Name} growls in frustration.");
                }
            }
            else
            {
                MoveTowards(target.X, target.Y, gameMap, entities);
            }

            foreach (Entity entity in entities)
            {
                gameMap.SetCellProperties(entity.X, entity.Y, gameMap.IsTransparent(entity.X, entity.Y), true);
            }

        }

        /// <summary>
        /// The distance to point.
        /// </summary>
        /// <param name="targetX">The X co-ordinate to check the distance to.</param>
        /// <param name="targetY">The Y co-ordinate to check the distance to.</param>
        /// <returns>The crow-flies distance (rounded down) to the point.</returns>
        public int Distance(int targetX, int targetY)
        {
            var dx = targetX - X;
            var dy = targetY - Y;

            return (int)Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
        }

        /// <summary>
        /// The distance to another <see cref="Entity"/>.
        /// </summary>
        /// <param name="other">The <see cref="Entity"/> to check the distance to.</param>
        /// <returns>The crow-flies distance (rounded down) to the <paramref name="other"/> <see cref="Entity"/>.</returns>
        public int DistanceTo(Entity other)
        {
            var dx = other.X - X;
            var dy = other.Y - Y;

            return (int)Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
        }

        /// <summary>
        /// Kill the specified monster dead.
        /// </summary>
        /// <param name="monster">An <see cref="Entity"/> to be killed dead.</param>
        /// <returns>A <see cref="Message"/> to be displayed.</returns>
        public static Message KillMonster(Entity monster)
        {
            var deathMessage = $"{monster.Name.First().ToString().ToUpper() + monster.Name.Substring(1)} is dead!";
            monster.Char = "corpse";
            monster.Color = "corpse";
            monster.Blocks = false;
            monster.Fighter = null;
            monster.AI = null;
            monster.Name = $"remains of {monster.Name}";
            monster.RenderOrder = Graphics.RenderOrder.CORPSE;

            return new Message(deathMessage, "#FFFF8000");
        }

        /// <summary>
        /// Kill the player dead.
        /// </summary>
        /// <param name="player">The player to be killed.</param>
        /// <returns>A <see cref="Message"/> to be displayed.</returns>
        public static Message KillPlayer(Entity player)
        {
            player.Char = "corpse";
            player.Color = "corpse";

            return new Message("You died!", "#FFFF0000");
        }
    }

    /// <summary>
    /// Functions pertaining to entities
    /// </summary>
    public static class Entities
    {
        /// <summary>
        /// Whether a blocking entity exists in a specific position.
        /// </summary>
        /// <param name="entities">A list of entities to work through.</param>
        /// <param name="destinationX">X co-ordinate to check.</param>
        /// <param name="destinationY">Y co-ordering to check.</param>
        /// <returns>If a blocking entity exists, then <see cref="CyberRogue.Core.Entity"/>, otherwise null.</returns>
        public static Entity BlockingAtLocation(List<Entity> entities, int destinationX, int destinationY)
        {
            foreach(Entity entity in entities)
            {
                if (entity.Blocks && entity.X == destinationX && entity.Y == destinationY)
                    return entity;
            }

            return null;
        }
    }
}
