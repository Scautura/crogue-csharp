﻿using System;
using RogueSharp;
using System.Collections.Generic;
using System.Linq;
using CyberRogue.Core;
using CyberRogue.Components;

namespace CyberRogue.Cartography
{
    /// <summary>
    /// A representation of a game map.
    /// </summary>
    public class GameMap : Map
    {
        private static Random rnd = new Random();
        
        /// <summary>
        /// Constructor creates a new uninitialized GameMap
        /// </summary>
        public GameMap() : base()
        { }

        /// <summary>
        /// Constructor creates a new GameMap and immediately initializes it
        /// </summary>
        /// <param name="width">How many Cells wide the Map will be</param>
        /// <param name="height">How many Cells tall the Map will be</param>
        public GameMap(int width, int height) : base(width, height)
        { }

        /// <summary>
        /// Carves the map out of the array.
        /// </summary>
        /// <param name="MaxRooms">The maximum number of rooms.</param>
        /// <param name="RoomMinSize">Minimum size of a room.</param>
        /// <param name="RoomMaxSize">Maximum size of a room.</param>
        /// <param name="player">The player <see cref="Core.Entity"/>.</param>
        /// <param name="entities">A list of entities.</param>
        /// <param name="maxMonstersPerRoom">Maximum number of monsters to be generated in a room.</param>
        /// <param name="maxItemsPerRoom">Maximum number of items to be generated in a room.</param>
        public void MakeMap(int MaxRooms, int RoomMinSize, int RoomMaxSize, Entity player, List<Entity> entities, int maxMonstersPerRoom, int maxItemsPerRoom)
        {
            List<Rectangle> rooms = new List<Rectangle>();

            Array mapGenerationChoice = Enum.GetValues(typeof(Constants.MapGeneration));

            switch ((Constants.MapGeneration)mapGenerationChoice.GetValue(rnd.Next(mapGenerationChoice.Length)))
            {
                case Constants.MapGeneration.BSP:

                    var bigMap = new Rectangle(0, 0, Width, Height);

                    var root = new Bsp(null, bigMap);
                    root.Split(RoomMinSize);

                    root.CreateRoom(this, RoomMinSize, Constants.FullRooms);

                    var spawnRoom = root.GetRoom();
                    player.X = spawnRoom.Center.X;
                    player.Y = spawnRoom.Center.Y;

                    break;

                default:
                    int NumRooms = 0;

                    for (int i = 0; i < MaxRooms; i++)
                    {
                        int w = rnd.Next(RoomMinSize, RoomMaxSize + 1);
                        int h = rnd.Next(RoomMinSize, RoomMaxSize + 1);

                        int x = rnd.Next(0, Width - w - 1);
                        int y = rnd.Next(0, Height - h - 1);

                        Rectangle NewRoom = new Rectangle(x, y, w, h);

                        bool intersect = rooms.Any(room => NewRoom.Intersects(room));

                        if (!intersect)
                        {
                            CreateRoom(NewRoom);

                            if (NumRooms == 0)
                            {
                                player.X = NewRoom.Center.X;
                                player.Y = NewRoom.Center.Y;
                            }
                            else
                            {
                                Rectangle OldRoom = rooms[NumRooms - 1];
                                if (rnd.Next(2) == 1)
                                {
                                    CreateHorTunnel(OldRoom.Center.X, NewRoom.Center.X, OldRoom.Center.Y);
                                    CreateVerTunnel(OldRoom.Center.Y, NewRoom.Center.Y, NewRoom.Center.X);
                                }
                                else
                                {
                                    CreateVerTunnel(OldRoom.Center.Y, NewRoom.Center.Y, OldRoom.Center.X);
                                    CreateHorTunnel(OldRoom.Center.X, NewRoom.Center.X, NewRoom.Center.Y);
                                }
                            }
                            PlaceEntities(NewRoom, entities, maxMonstersPerRoom, maxItemsPerRoom);

                            rooms.Add(NewRoom);
                            NumRooms++;
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Determines whether the specified co-ordinate is blocked.
        /// </summary>
        /// <param name="x">The x co-ordinate to check.</param>
        /// <param name="y">The y co-ordinate to check.</param>
        /// <returns>
        ///   <c>true</c> if the specified x, y is blocked; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBlocked(int x, int y)
        {
            if (IsWalkable(x,y))
                return false;

            return true;
        }

        /// <summary>
        /// Creates a horizontal tunnel.
        /// </summary>
        /// <param name="x1">The starting x co-ordinate.</param>
        /// <param name="x2">The finishing x co-ordinate.</param>
        /// <param name="y">The y co-ordinate.</param>
        public void CreateHorTunnel(int x1, int x2, int y)
        {
            for (int x = Math.Min(x1, x2); x <= Math.Max(x1, x2); x++)
            {
                SetCellProperties(x, y, true, true);
            }
        }

        /// <summary>
        /// Creates a vertical tunnel.
        /// </summary>
        /// <param name="y1">The starting y co-ordinate.</param>
        /// <param name="y2">The finishing y co-ordinate.</param>
        /// <param name="x">The x co-ordinate.</param>
        public void CreateVerTunnel(int y1, int y2, int x)
        {
            for (int y = Math.Min(y1, y2); y <= Math.Max(y1, y2); y++)
            {
                SetCellProperties(x, y, true, true);
            }
        }

        /// <summary>
        /// Creates a room.
        /// </summary>
        /// <param name="room">The rectangle representing the room to create.</param>
        public void CreateRoom(Rectangle room)
        {
            for (int x = room.Left + 1; x < room.Right - 1; x++)
            {
                for (int y = room.Top + 1; y < room.Bottom - 1; y++)
                {
                    SetCellProperties(x, y, true, true);
                }
            }
        }

        /// <summary>
        /// Place monsters into a room.
        /// </summary>
        /// <param name="room">A <see cref="RogueSharp.Rectangle"/> representing a room.</param>
        /// <param name="entities">A list of entities.</param>
        /// <param name="maxMonstersPerRoom">Maximum number of monsters in a room.</param>
        /// <param name="maxItemsPerRoom">Maximum number of items in a room.</param>
        public void PlaceEntities(Rectangle room, List<Entity> entities, int maxMonstersPerRoom, int maxItemsPerRoom)
        {
            int number_of_monsters = rnd.Next(0, maxMonstersPerRoom + 1);
            int number_of_items = rnd.Next(0, maxItemsPerRoom + 1);

            for (int i = 0; i < number_of_monsters; i++)
            {
                int x = rnd.Next(room.Left + 1, room.Right - 1);
                int y = rnd.Next(room.Top + 1, room.Bottom - 1);

                if (!entities.Any(entity => entity.X == x && entity.Y == y))
                {
                    Entity monster;
                    if (rnd.Next(0, 100) < 80)
                    {
                        var fighterComponent = new Fighter(hp: 10, defense: 0, power: 3);
                        var aiComponent = new AI.BasicMonster();

                        monster = new Entity(x, y, "orcTile", "orc", "Orc", blocks: true, renderOrder: Graphics.RenderOrder.ACTOR, fighter: fighterComponent, ai: aiComponent);
                    }
                    else
                    {
                        var fighterComponent = new Fighter(hp: 16, defense: 1, power: 4);
                        var aiComponent = new AI.BasicMonster();

                        monster = new Entity(x, y, "trollTile", "troll", "Troll", blocks: true, renderOrder: Graphics.RenderOrder.ACTOR, fighter: fighterComponent, ai: aiComponent);
                    }

                    entities.Add(monster);
                }
            }

            for (int i = 0; i < number_of_items; i++)
            {
                int x = rnd.Next(room.Left + 1, room.Right - 1);
                int y = rnd.Next(room.Top + 1, room.Bottom - 1);
                
                if(!entities.Any(entity => entity.X == x && entity.Y == y))
                {
                    Entity item;
                    var itemChance = rnd.Next(0, 100);

                    if (itemChance < 70)
                    {
                        var itemComponent = new Item(ItemFunctions.Heal, new Dictionary<string, object> { { "amount", 4 } });
                        item = new Entity(x, y, "potionTile", "potion", "Healing Potion", renderOrder: Graphics.RenderOrder.ITEM, item: itemComponent);
                    }
                    else if (itemChance < 80)
                    {
                        var itemComponent = new Item(ItemFunctions.CastFireball, new Dictionary<string, object> { { "damage", 12 }, { "radius", 3 } }, targeting: true, targetingMessage: new Message("Left-click a target tile for the fireball, or right-click to cancel.", Constants.Colors["light_cyan"]));
                        item = new Entity(x, y, "scrollTile", "scroll", "Fireball Scroll", renderOrder: Graphics.RenderOrder.ITEM, item: itemComponent);
                    }
                    else if (itemChance < 90)
                    {
                        var itemComponent = new Item(ItemFunctions.CastConfuse, new Dictionary<string, object> { }, targeting: true, targetingMessage: new Message("Left-click an enemy to confuse it, or right-click to cancel.", Constants.Colors["light_cyan"]));
                        item = new Entity(x, y, "scrollTile", "scroll", "Confuse Scroll", renderOrder: Graphics.RenderOrder.ITEM, item: itemComponent);
                    }
                    else
                    {
                        var itemComponent = new Item(ItemFunctions.CastLightning, new Dictionary<string, object> { { "damage", 20 }, { "maximum_range", 5 } });
                        item = new Entity(x, y, "scrollTile", "scroll", "Lightning Scroll", renderOrder: Graphics.RenderOrder.ITEM, item: itemComponent);
                    }

                    entities.Add(item);
                }
            }
        }
    }
}
