﻿using RogueSharp;
using System;

/* Original code adapted from https://gamedevelopment.tutsplus.com/tutorials/how-to-use-bsp-trees-to-generate-game-maps--gamedev-12268
 * This version based on https://github.com/pjk21/roguelikedev-does-the-complete-roguelike-tutorial/blob/master/Roguelike/Roguelike/World/MapGeneration/BspMapGenerator.cs
 * Used under MIT license.
 */

namespace CyberRogue.Cartography
{

    /// <summary>
    /// Binary Space Partitioning base class for a node/leaf.
    /// </summary>
    public class Bsp
    {
        /// <summary>
        /// The parent node.
        /// </summary>
        public Bsp Parent { get; }

        /// <summary>
        /// The first (left) child node.
        /// </summary>
        public Bsp ChildA { get; private set; }

        /// <summary>
        /// The second (right) child node.
        /// </summary>
        public Bsp ChildB { get; private set; }

        /// <summary>
        /// The area to be split in terms of a <see cref="RogueSharp.Rectangle"/>.
        /// </summary>
        public Rectangle Size { get; set; }

        /// <summary>
        /// The room contained in an end node (is an empty <see cref="RogueSharp.Rectangle"/> in parent nodes).
        /// </summary>
        public Rectangle Room { get; set; }

        private static Random rnd = new Random();

        /// <summary>
        /// Constructor for a node.
        /// </summary>
        /// <param name="parent">The parent node to this node.</param>
        /// <param name="size">A <see cref="RogueSharp.Rectangle"/> of the contained area.</param>
        public Bsp(Bsp parent, Rectangle size)
        {
            Parent = parent;
            Size = size;
        }

        /// <summary>
        /// Split a node into smaller nodes, until the smallest size is reached.
        /// </summary>
        /// <param name="RoomMinSize">Minimum size of a node.</param>
        public void Split(int RoomMinSize)
        {
            int direction;

            if (Size.Width > Size.Height)
                direction = 0;
            else if (Size.Height > Size.Width)
                direction = 1;
            else
                direction = rnd.Next(0, 2);

            if (direction == 0)
            {
                var childRoomWidth = (Size.Width / 2) + rnd.Next(-3, 4);

                if (childRoomWidth >= RoomMinSize && Size.Width - childRoomWidth >= RoomMinSize)
                {
                    ChildA = new Bsp(this, new Rectangle(Size.Left, Size.Top, childRoomWidth, Size.Height));
                    ChildA.Split(RoomMinSize);

                    ChildB = new Bsp(this, new Rectangle(Size.Left + childRoomWidth, Size.Top, Size.Width - childRoomWidth, Size.Height));
                    ChildB.Split(RoomMinSize);
                }
            }
            else
            {
                var childRoomHeight = (Size.Height / 2) + rnd.Next(-3, 4);

                if (childRoomHeight >= RoomMinSize && Size.Height - childRoomHeight >= RoomMinSize)
                {
                    ChildA = new Bsp(this, new Rectangle(Size.Left, Size.Top, Size.Width, childRoomHeight));
                    ChildA.Split(RoomMinSize);

                    ChildB = new Bsp(this, new Rectangle(Size.Left, Size.Top + childRoomHeight, Size.Width, Size.Height - childRoomHeight));
                    ChildB.Split(RoomMinSize);
                }
            }
        }

        /// <summary>
        /// Create a room within a node (or child nodes) until all child nodes have rooms.
        /// </summary>
        /// <param name="map">A <see cref="CyberRogue.Cartography.GameMap"/> object to create rooms in.</param>
        /// <param name="RoomMinSize">Minimum size for a room within a node.</param>
        /// <param name="fullRooms">Whether to fill the the node or not.</param>
        public void CreateRoom(GameMap map, int RoomMinSize, bool fullRooms = false)
        {
            if (ChildA != null || ChildB != null)
            {
                ChildA?.CreateRoom(map, RoomMinSize, fullRooms);
                ChildB?.CreateRoom(map, RoomMinSize, fullRooms);

                if (ChildA != null && ChildB != null)
                {
                    var roomA = ChildA.GetRoom();
                    var roomB = ChildB.GetRoom();

                    if (roomA.Left == roomB.Left || roomA.Right == roomB.Right)
                    {
                        if (roomA.Width >= roomB.Width)
                        {
                            map.CreateVerTunnel(roomA.Center.Y, roomB.Center.Y, roomB.Center.X);
                        }
                        else
                        {
                            map.CreateVerTunnel(roomA.Center.Y, roomB.Center.Y, roomA.Center.X);
                        }
                    }
                    else if (roomA.Top == roomB.Top || roomA.Bottom == roomB.Bottom)
                    {
                        if (roomA.Height >= roomB.Height)
                        {
                            map.CreateHorTunnel(roomA.Center.X, roomB.Center.X, roomB.Center.Y);
                        }
                        else
                        {
                            map.CreateHorTunnel(roomA.Center.X, roomB.Center.X, roomA.Center.Y);
                        }
                    }
                    else
                    {
                        if (roomA.Width >= roomB.Width)
                        {
                            map.CreateVerTunnel(roomA.Center.Y, roomB.Center.Y, roomB.Center.X);
                        }
                        else
                        {
                            map.CreateVerTunnel(roomA.Center.Y, roomB.Center.Y, roomA.Center.X);
                        }

                        if (roomA.Height >= roomB.Height)
                        {
                            map.CreateHorTunnel(roomA.Center.X, roomB.Center.X, roomB.Center.Y);
                        }
                        else
                        {
                            map.CreateHorTunnel(roomA.Center.X, roomB.Center.X, roomA.Center.Y);
                        }
                    }
                }
            }
            else
            {
                int maxShrinkX = fullRooms ? 0 : Size.Width - RoomMinSize;
                int maxShrinkY = fullRooms ? 0 : Size.Height - RoomMinSize;

                int width = Size.Width - rnd.Next(0, maxShrinkX);
                int height = Size.Height - rnd.Next(0, maxShrinkY);

                int x = rnd.Next(Size.Left, Size.Right - width);
                int y = rnd.Next(Size.Top, Size.Bottom - height);

                Rectangle room = new Rectangle(x, y, width, height);
                Room = room;

                map.CreateRoom(room);

                map.PlaceEntities(room, CyberRogue.Program.entities, CyberRogue.Core.Constants.MaxMonstersPerRoom, CyberRogue.Core.Constants.MaxItemsPerRoom);
            }
        }

        /// <summary>
        /// Get a room within the given node. Works down to a bottom node until a room is found.
        /// </summary>
        /// <returns>A <see cref="RogueSharp.Rectangle"/> object for a room within the node.</returns>
        public Rectangle GetRoom()
        {
            if (Room != Rectangle.Empty)
            {
                return Room;
            }
            else
            {
                if (ChildA != null && ChildB == null)
                {
                    return ChildA.GetRoom();
                }
                else if (ChildA == null && ChildB != null)
                {
                    return ChildB.GetRoom();
                }
                else
                {
                    if (rnd.Next(0, 2) == 0)
                    {
                        return ChildA.GetRoom();
                    }
                    else
                    {
                        return ChildB.GetRoom();
                    }
                }
            }
        }
    }
}
