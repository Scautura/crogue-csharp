﻿using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.Components
{
    /// <summary>
    /// Fighter class for all entities that fight.
    /// </summary>
    public class Fighter
    {
        /// <summary>
        /// Current health points.
        /// </summary>
        public int HP;

        /// <summary>
        /// Defense value.
        /// </summary>
        public int Defense;

        /// <summary>
        /// Maximum HP value.
        /// </summary>
        public int MaxHP;

        /// <summary>
        /// Power value.
        /// </summary>
        public int Power;

        /// <summary>
        /// The <see cref="Entity"/> that owns this Fighter object.
        /// </summary>
        public Entity owner;

        /// <summary>
        /// Initialize a fighter with given values.
        /// </summary>
        /// <param name="hp">Initial and maximum HP.</param>
        /// <param name="defense">Initial defense value.</param>
        /// <param name="power">Initial power value.</param>
        public Fighter(int hp, int defense, int power)
        {
            HP = hp;
            MaxHP = hp;
            Defense = defense;
            Power = power;
        }

        /// <summary>
        /// Have entity take damage.
        /// </summary>
        /// <param name="amount">Amount of damage to take.</param>
        /// <returns>A list of tuples containing status/messages.</returns>
        public List<Tuple<string, dynamic>> TakeDamage(int amount)
        {
            List<Tuple<string, dynamic>> results=new List<Tuple<string, dynamic>>();
            HP -= amount;

            if(HP<=0)
            {
                results.Add(new Tuple<string, dynamic>("dead", owner));
            }

            return results;
        }
        /// <summary>
        /// Attack a target entity.
        /// </summary>
        /// <param name="target">The target to attack.</param>
        /// <returns>A list of tuples containing status/messages.</returns>
        public List<Tuple<string, dynamic>> Attack(Entity target)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();
            var damage = Power - target.Fighter.Defense;

            if(damage>0)
            {
                
                results.Add(new Tuple<string, dynamic>("message", new Message($"{owner.Name.First().ToString().ToUpper() + owner.Name.Substring(1)} attacks {target.Name} for {damage} hit points.", "white")));
                results.AddRange(target.Fighter.TakeDamage(damage));
            }
            else
            {
                results.Add(new Tuple<string, dynamic>("message", new Message($"{owner.Name.First().ToString().ToUpper() + owner.Name.Substring(1)} attacks {target.Name} but does no damage.", "white")));
            }

            return results;
        }

        public void Heal(int amount)
        {
            HP += amount;

            if(HP>MaxHP)
            {
                HP = MaxHP;
            }
        }
    }
}
