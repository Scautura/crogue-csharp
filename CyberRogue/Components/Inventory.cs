﻿using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.Components
{
    public class Inventory
    {
        private int Capacity;

        public List<Entity> Items;

        public Entity owner;

        public Inventory(int capacity)
        {
            Capacity = capacity;
            Items = new List<Entity>();
        }

        public List<Tuple<string, dynamic>> AddItem(Entity item)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            if (Items.Count >= Capacity)
            {
                results.Add(new Tuple<string, dynamic>("item_added", null));
                results.Add(new Tuple<string, dynamic>("message", new Message("You cannot carry any more, your inventory is full", Constants.Colors["yellow"])));
            }
            else
            {
                results.Add(new Tuple<string, dynamic>("item_added", item));
                results.Add(new Tuple<string, dynamic>("message", new Message($"You pick up the {item.Name}!", Constants.Colors["blue"])));

                Items.Add(item);
            }

            return results;
        }

        public List<Tuple<string, dynamic>> Use(Entity itemEntity, Dictionary<string, object> kwargs = null)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            var itemComponent = itemEntity.Item;

            if (itemComponent.UseFunction == null)
            {
                results.Add(new Tuple<string, dynamic>("message", new Message($"The {itemEntity.Name} cannot be used", Constants.Colors["yellow"])));
            }
            else
            {
                if (itemComponent.Targeting && !(kwargs.ContainsKey("target_x") || kwargs.ContainsKey("target_y")))
                {
                    results.Add(new Tuple<string, dynamic>("targeting", itemEntity));
                }
                else
                {
                    var itemUseResults = itemComponent.UseFunction(owner, kwargs);

                    foreach (var itemUseResult in itemUseResults)
                    {
                        if (itemUseResult.Item1 == "consumed" && itemUseResult.Item2 == true)
                        {
                            RemoveItem(itemEntity);
                        }
                    }

                    results.AddRange(itemUseResults);
                }
            }

            return results;
        }

        public void RemoveItem(Entity item)
        {
            Items.Remove(item);
        }

        public List<Tuple<string, dynamic>> DropItem(Entity item)
        {
            List<Tuple<string, dynamic>> results = new List<Tuple<string, dynamic>>();

            item.X = owner.X;
            item.Y = owner.Y;

            RemoveItem(item);
            results.Add(new Tuple<string, dynamic>("item_dropped", item));
            results.Add(new Tuple<string, dynamic>($"You dropped the {item.Name}", Constants.Colors["yellow"]));

            return results;
        }
    }
}
