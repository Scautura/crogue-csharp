﻿using CyberRogue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberRogue.Components
{
    public class Item
    {
        public Entity owner;

        public Func<Entity, Dictionary<string, dynamic>, List<Tuple<string, dynamic>>> UseFunction;
        public bool Targeting;
        public Message TargetingMessage;

        public Item(Func<Entity, Dictionary<string, dynamic>, List<Tuple<string, dynamic>>> useFunction = null, Dictionary<string, dynamic> kwargs = null, bool targeting = false, Message targetingMessage = null)
        {
            UseFunction = (Entity entity, Dictionary<string, dynamic> args) => useFunction(entity, args.Concat(kwargs).GroupBy(d => d.Key).ToDictionary(k => k.Key, k => k.First().Value));
            Targeting = targeting;
            TargetingMessage = targetingMessage;
        }
    }
}
