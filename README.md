# r/RoguelikeDev Does The Complete Roguelike Tutorial

![RoguelikeDev Does the Complete Roguelike Tutorial Event Logo](https://i.imgur.com/ksc9EW3.png)

[![Build status](https://ci.appveyor.com/api/projects/status/bcd3a80ob99vp7x9?svg=true)](https://ci.appveyor.com/project/Scautura/crogue-csharp)

At [r/roguelikedev](https://www.reddit.com/r/roguelikedev/) we're doing a dev-along following [The Complete Roguelike Tutorial](http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod).

This version is by Scautura, working with C#, [BearLibTerminal](http://foo.wyrd.name/en:bearlibterminal) and [RogueSharp](https://roguesharp.wordpress.com/)

## If you would like to participate on GitHub

* [x][Sign up for a free personal account](https://github.com/signup/free) if you don't already have one.
* [x]Fork [this repository](https://github.com/aaron-santos/roguelikedev-does-the-complete-roguelike-tutorial) to your account.
* [x]Clone the repository on your computer and follow the tutorial.
* [x]Follow along with the [weekly posts](https://www.reddit.com/r/roguelikedev/search?q=TCRT&restrict_sr=on).
* [x]Update the `README.md` file to include a description of your game, how/where to play/download it, how to build/compile it, what dependencies it has, etc.
* [ ]Share your game on the final week.

## It's dangerous to go alone

If you're **new to Git, BitBucket, or version control**…

* [Git Documentation](https://git-scm.com/documentation) - everything you need to know about version control, and how to get started with Git.
* [BitBucket Help](https://confluence.atlassian.com/bitbucket/bitbucket-cloud-documentation-221448814.html) - everything you need to know about BitBucket.